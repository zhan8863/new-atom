import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import Hello  from './containers/Hello'
import { createStore } from 'redux'
import { enthusiasm } from './reducers/index'
import { StoreState } from './types/index'
import { Router, Route, browserHistory } from 'react-router'
import './index.css'
const store = createStore<StoreState>(enthusiasm, {
  enthusiasmLevel: 1,
  languageName: 'TypeScript',
})
ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory} >
      <Route path="/" component={Hello}/>
    </Router>
  </Provider>,
  document.getElementById('root') as HTMLElement
)
console.log(1)