import * as React from 'react'
import './Hello.css'
import * as electron from 'electron'
import { WebviewTag, Details } from 'electron'
const {ipcRenderer} = require('electron')
export interface Props {
  name: string
  enthusiasmLevel?: number
  onIncrement?: () => void
  onDecrement?: () => void
}

function Hello({ name, enthusiasmLevel = 1, onIncrement, onDecrement }: Props) {
  if (enthusiasmLevel <= 0) {
    throw new Error('You could be a little more enthusiastic. :D')
  }

  // let webview = (document.querySelector('webview') as WebviewTag)
  // webview.addEventListener('did-finish-load', async () => {
  //   console.log(webview.getWebContents(), webview.getWebContents().session)
  //   const session = webview.getWebContents().session
  //   webview.getWebContents().session.cookies.get({}, function(err: any, args: any) {
  //     console.log(args)
  //   })
  //   // fetch('http://app.men.test.mi.com/product/category?parentId=0&source=hipos&orgId=MI0101', {
  //   //   credentials : 'include'
  //   // })
  // })
  
  return (
    <div className="hello">
      <div className="greeting">
        Hello haa {name + getExclamationMarks(enthusiasmLevel)}
      </div>
      <div>
        <button onClick={onDecrement}>-</button>
        <button onClick={onIncrement}>+</button>
      </div>
    </div>
  )
}

export default Hello

function getExclamationMarks(numChars: number) {
  return Array(numChars + 1).join('!')
}
