const merge = require('webpack-merge')
import * as webpack from 'webpack'
const common = require('./webpack.common')
import * as path from 'path'
const config: webpack.Configuration = merge(common, {
  entry: [
    './src/index.tsx',
  ],
  module: {
    rules: [{
      test: /\.tsx?$/,
      loaders: [
        'awesome-typescript-loader'
      ],
      exclude: path.resolve(__dirname, 'node_modules'),
      include: path.resolve(__dirname, 'src'),
    },
  ]
  },
})
export default config