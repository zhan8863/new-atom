import * as webpack from 'webpack'
import * as path from 'path'
import * as ExtractTextPlugin from 'extract-text-webpack-plugin'
import * as HtmlWebpackPlugin from 'html-webpack-plugin'
const CleanWebpackPlugin = require('clean-webpack-plugin')
const CSSExtract = new ExtractTextPlugin('styles.css')
module.exports = {
  target: 'electron-renderer',
  output: {
    path: path.join(__dirname, 'output'),
    filename: '[name].bundle.js'
  },
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js']
  },
  module: {
    rules: [{
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader'
      },
      // {
      //   test: /\.scss$/,
      //   use: ExtractTextPlugin.extract({
      //     fallback: 'style-loader',
      //     use: ['css-loader', 'sass-loader']
      //   })
      // }
      {
        test: /\.css$/,
        use: [{
            loader: 'style-loader' // 将 JS 字符串生成为 style 节点
        }, {
            loader: 'css-loader' // 将 CSS 转化成 CommonJS 模块
        }]
      },
      {
        test: /\.scss$/,
        use: [{
            loader: 'style-loader' // 将 JS 字符串生成为 style 节点
        }, {
            loader: 'css-loader' // 将 CSS 转化成 CommonJS 模块
        }, {
            loader: 'sass-loader' // 将 Sass 编译成 CSS
        }]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin('output'),
    CSSExtract,
    new webpack.NamedModulesPlugin(),
    new HtmlWebpackPlugin({
      title: 'react-hot-ts',
      chunksSortMode: 'dependency',
      template: path.resolve(__dirname, './public/index.html')
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common' // 指定公共 bundle 的名称。
    })
  ]
}