import './setup'
import * as React from 'react'
import Input from '../src/components/Input'
import { shallow, render } from 'enzyme'

const setup = () => {
  // 模拟 props
  const props = {
    // Jest 提供的mock 函数
    onAddClick: jest.fn()
  }
  // 通过 enzyme 提供的 shallow(浅渲染) 创建组件
  const wrapper = shallow(<Input/>)
  return {
    props,
    wrapper
  }
}
describe('AddTodoView', () => {
  const { wrapper, props } = setup()
  // 通过查找存在 Input,测试组件正常渲染
  it('Input Component should be render', () => {
    expect(wrapper.find('input').exists())
  })
})