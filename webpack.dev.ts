import * as webpack from 'webpack'
import * as path from 'path'
const merge = require('webpack-merge')
const common = require('./webpack.common')

const config: webpack.Configuration = merge(common, {
  entry: [
    'react-hot-loader/patch',
    './src/index.tsx',
  ],
  module: {
    rules: [{
      test: /\.tsx?$/,
      loaders: [
        'react-hot-loader/webpack',
        'awesome-typescript-loader'
      ],
      exclude: path.resolve(__dirname, 'node_modules'),
      include: path.resolve(__dirname, 'src'),
    }
  ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
  ],
  devtool: 'inline-source-map',
  devServer: {
    hot: true,
    port: 8081
  }
})
console.log(config)
export default config