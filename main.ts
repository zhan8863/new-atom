'use strict'

const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
const {ipcMain} = require('electron')

let mainWindow = null

app.on('window-all-closed', () => {
  app.quit()
})
let winProps = {
  width: 1000,
  height: 800
}
app.on('ready', () => {
  mainWindow = new BrowserWindow(winProps)
  let url = process.env.NODE_ENV === 'development' ? `http://localhost:8081` : `file://${__dirname}/output/index.html`
  console.log(url)
  // mainWindow.loadURL(`file://${__dirname}/public/index.html`);
  mainWindow.loadURL(url)
  // mainWindow.loadURL(`file://${__dirname}/index.html`)

  mainWindow.webContents.openDevTools()
  
  mainWindow.on('closed', () => {
    mainWindow = null
  })
})
