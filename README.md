## include
- electron 1.7.9 +
- typescript
- jest
- enzyme
- react
- react-hot-loader
- sass-loader
- webpack


## 开发

```
第一步：npm run dev     //终端一 启动 dev 服务
第二步：npm run electron        //终端二 启动 electron，依赖于前面的 dev 服务
```

## 构建
`npm run build`



**如果不需要electron，只是在普通的浏览器中访问，只需将webpack.config.ts中的target:'electron-render'去掉即可**